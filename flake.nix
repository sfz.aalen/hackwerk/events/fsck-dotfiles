# /etc/nixos/flake.nix
{
  description = "A2 POC Server";

  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
    };
  };

  outputs = { self, nixpkgs, ... }: {
    nixosConfigurations = {
      hokuspocus = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./hardware-configuration.nix
          ./modules
        ];
      };
    };
  };
}
