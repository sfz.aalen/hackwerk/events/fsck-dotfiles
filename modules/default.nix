{ config, pkgs, ... }:

{
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  environment.systemPackages = with pkgs; [
    sngrep
    postgresql
  ];

  imports =
    [
      ./virtualisation.nix
      ./dhcpd.nix
      ./vpn.nix
      ./omm.nix
      ./generic
      ./containers
      ./monitoring.nix
    ];
}

