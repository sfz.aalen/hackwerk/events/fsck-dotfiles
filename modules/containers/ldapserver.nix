{
  virtualisation.oci-containers.containers = {
    ldapserver = {
      image =
        "docker.sfz-aalen.space/sfz.aalen/hackwerk/events/guru3-ldap-server:1699758207";
      ports = [
        "10389:10389"
      ];
      autoStart = true;
      environmentFiles = [
        /asterisk/ldap/env
      ];
      volumes = [
        "/asterisk/ldap/data/:/data/:rw"
      ];
    };
  };
}