{
  virtualisation.oci-containers.containers = {
    asterisk-db = {
      image =
        "docker.io/library/postgres:16";
      ports = [
        "5432:5432"
      ];
      autoStart = true;
      environmentFiles = [
        /asterisk/db/env
      ];
      volumes = [
        "/asterisk/db/data/:/var/lib/postgresql/data/:rw"
      ];
    };
  };
}