{
  virtualisation.oci-containers.containers = {
    hexidian = {
      image =
        "docker.sfz-aalen.space/sfz.aalen/hackwerk/events/hexidian:1699725610";
      ports = [
        "4242:4242"
      ];
      autoStart = true;
      environmentFiles = [
        /asterisk/hexidian/env
        /asterisk/db/env
      ];
      volumes = [
        "/asterisk/hexidian/config/:/config/:ro"
      ];
    };
  };
}