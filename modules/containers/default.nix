{ config, pkgs, ... }:

{
  imports =
    [
      ./asterisk.nix
      ./db.nix
      ./hexidian.nix
      ./ldapserver.nix
    ];

    virtualisation.oci-containers.backend = "docker";
}