{
  virtualisation.oci-containers.containers = {
    pbx = {
      image =
        "docker.sfz-aalen.space/sfz.aalen/hackwerk/events/asterisk-docker:1698057376";
      ports = [
        #"5432:5432"
        "5060:5060/tcp"
        "5060:5060/udp"
        "5038:5038"
        "8088:8088"
        "10000-11000:10000-11000/udp"
        "1314"
        "8095:8095"
      ];
      autoStart = true;
      volumes = [
        "/asterisk/config/:/etc/asterisk:rw"
        "/asterisk/config/odbc/odbc.ini:/etc/odbc.ini:ro"
        "/asterisk/config/odbc/odbcinst.ini:/etc/odbcinst.ini:ro"
        "/asterisk/sounds/:/var/lib/asterisk/sounds/:ro"
      ];
      extraOptions = [ "--network=host" ];
    };
  };
}