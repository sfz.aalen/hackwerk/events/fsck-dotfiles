{
   systemd.services.omm = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "docker-pbx.service" "docker-asterisk-db.service"];
      description = "Start the Mitel Open Mibility Manager.";
      environment = {
        LD_LIBRARY_PATH="/asterisk/mitel-omm-nix/lib/:$LD_LIBRARY_PATH";
      };
      serviceConfig = {
        ExecStart = "/asterisk/mitel-omm-nix/bin/SIP-DECT"; 
        WorkingDirectory="/asterisk/mitel-omm-nix/";
        StandardInput="tty"; # Needed, else it will spay the logs with the interactive shell
        TTYPath="/dev/tty2";
        TTYReset="yes";
        TTYVHangup="yes";
      };
   };
}