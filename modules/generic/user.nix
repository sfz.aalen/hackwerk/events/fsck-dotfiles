{ config, pkgs, ... }:
{
    users.users.admin = {
        isNormalUser = true;
        initialPassword = "password";
        extraGroups = [ "wheel" "podman" "docker" ];
        openssh.authorizedKeys.keys = [
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCvSJQF1eG4uMJD0eWOsn9XUzSA2oVn6YqkazmTEtOYa0bA/4yvo3iCR1EMrolmKxmrjeio6UpZnKyelEeTWRudIf6quLW/hFGiuRrhsgl4qg+XwKO6zCZ2Kx0q158Y6gJGnZ5MTl+0FNfnrCYn4nOJPO/HZT8lBwi41Gb7DgDqgQKbZc6pNF5jfF2wLY2rsRJzJUYmb2LW6jgn3ffxb2sEFxK/io7oUIISVwHd4fM7uxgw5ErgevQ0wpy1M1Pi+Zag2+BjmiZAgSf+m7mq28+vgxQAzIMncdJR73zlcbLZBlxlpmw5PAeII1sZmFFKagXaYKvxC4ScS3oeUMH0TjbGfvABW/j5tgtGs0cjogjS8f/D8UVsPYg9dHKf6tZ3SaAKG2VCReDL45e+1n0d30kjKaeU5D62Tk53M+qMwYz/M60UjtvgiiRUWPJ4phKK+LRLlj+bXiDmcLOJ1HJ5/5oV0HYmnok3XqBYrUYoAHi00ogvIbFSzVwg9NIxwSX1KSs= luca@firewall.sfz.lab"
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDCVACVA90ZSl88NfFC1QVIjydwK2qK3TXXcoRRtkBZ3e3FrLbRs0SmYLyhK9gMHUL6FAc7Lpn5rWmJdpuAlpH0dmacGd17gJksPubi7tptZ23s8cNXzC8cWp5Fg/3cmVpo7Wbpiwq9U3Fr8teyTr7T+q0NLosHzUe2HHSvPv5OiDsWHbQ9T5qFGtwl3/FAUtLU+Csn90ULKDukkoEtn02LOdSKTjWFJe7QrUkVma19MHDVoREKuqCR1eaYO6ORMCLCnE4Wo6BTF0kr2JbiYAnr88Y3B0ntKBpdrlfJgMFGQ26bxbp8xblxBCJlzEO4HJCoNvwNwe+fF4SEyQ8cwV8GH7V3FbTqmCBilFZseoU8UF9786+Q9EVv0E2rdSUQIZp2OdNjZwRBt2U4Iu3iPSarTE8r8U8705Gg1of3xaTk+7chAFpjBHxBZKjwxyUS0xB7aPRRZ0KUeUonLieceBQli69ZdgK78mW+QYdByLFk9V85oQYvhQ9AD/JC8rxDot8= jakob@jakob-yoga"
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGSiN07Iu7cwHH3kC/KPeWE1Jv0Puaj1iwqrGknEfpVL blackdotraven (gitlab.com)"
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCaHc2J6Rc6Fv2oxOmgnXato1J1ccZSt3YAD5w0yM2k2RDO1Vpk+2MzFHBdK458MBIUKeLYvcPcbq2nUVXNffq2uVsGTPU2s+MwTwM9Qd5C8EQQpuLMUu4z2n3SCx3Lr4vReVROa+raTDRRwNQG3Gkw+YOcng0r5P8tpS//8QnL9RBDaOTJsQM/IsGircAYB43h7tWDEXkyoF8D2iKabYGDP5iEhByLrA0Cy5d3ReMR33+SxS5HN0uqqwvlOglgesRiCjNQTX4s5hcEU3lW+C5cUpb+3eJe6dB0Ho4QmcbkWjKUf8ITOKI8pmMrVSHW7iuQE0WXj+KDDxKCt1rXowXb14+n/Y7pjxZOxoQTYkMBRPfcZNJ/FUxJVgOz0zDFSZ23jRcgosK1ecXs7Afjx5DHewrDIdTcul0Bo0tcNtnSjQzJ4zrN9i2OfEY6J+DpTUqK/sgxcBO3a1gw9wGJ/qhtv1jSp1cMa29kX+Iujcm4HmzS5UCAsnzkYH4qtvMcp90= luca@venus"
        ];
    };
    security.sudo.wheelNeedsPassword = false;
}