{ pkgs, ... }: {
  environment.shellAliases = {
    system-update = ''nixos-rebuild switch --flake gitlab:sfz.aalen%2Fhackwerk%2Fevents/fsck-dotfiles/$(curl -s https://gitlab.com/api/v4/projects/57476507/repository/commits/ | ${pkgs.jq}/bin/jq -r -M ".[0].id")#hokuspocus'';
  };
  environment.shellInit = ''
    export HISTCONTROL=ignoreboth:erasedups
  '';
}

