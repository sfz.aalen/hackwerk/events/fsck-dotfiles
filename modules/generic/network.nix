{ lib, ... }:
{
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.
  networking.hostName = "poc-server";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.useDHCP = lib.mkDefault true;
  
  networking = {
    interfaces = {
      enp1s0.ipv4.addresses = [{ # PCI
        address = "10.40.42.10";
        prefixLength = 24;
      }];
      enp0s31f6.useDHCP = lib.mkDefault true; # Onboard
    };
    usePredictableInterfaceNames = true;
    defaultGateway = "10.40.42.1";
    nameservers = ["1.1.1.1" "9.9.9.9"];
  };
  
  networking.firewall.enable = false;
}
